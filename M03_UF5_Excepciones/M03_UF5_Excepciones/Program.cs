﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace M03_UF5_Excepciones
{
    class Program
    {
        static void Main(string[] args)
        {
            Program p = new Program();
            p.Inici();
            
        }

    // Inici del programa on s'executa el menu
    void Inici()
    {
        bool fin;
        do
        {
            Console.Clear();
            MostrarMenu();
            string opcion = Console.ReadLine();
            fin = CallExercice(opcion);
        } while (!fin);

    }
    
    // Es mostra els exercicis que es poden fer
    void MostrarMenu()
    {
        Console.WriteLine("Selecciona l'exercici");
        Console.WriteLine("[1]: Exercici 1");
        Console.WriteLine("[2]: Exercici 2");
        Console.WriteLine("[3]: Exercici 3");
        Console.WriteLine("[4]: Exercici 4");
        Console.WriteLine("[0]: Sortir");
    }

    //Permet seleccionar l'exercici per jugar
    bool CallExercice(string opcion)
    {
        Console.Clear();
        switch (opcion)
        {
            case "1":
                DivideixoCero();
                break;
            case "2":
                aDoubleoU();
                break;
            case "3":
                FtxerException();
                break;
            case "4":
                LliureAplic();
                break;
            
            case "0":
                return true;

            default:
                Console.WriteLine("Esta opció no existeix");
                break;
        }

        return false;
    }


        //Introdueix dos paràmetres (el dividend i el divisor) i el metode retorna el resultat de la divisió, o 0 en cas que el denominador sigui 0
        public void DivideixoCero()
        {

            try
            {
                int num1 = Convert.ToInt32(Console.ReadLine());
                int num2 = Convert.ToInt32(Console.ReadLine());
                int result = num1 / num2;
                Console.WriteLine(result);
            }
            catch (ArithmeticException e)
            {

                Console.WriteLine(e);
            }
            catch (Exception e2)
            {
                Console.WriteLine(e2);
            }
            Console.WriteLine("Finalizado");
            Console.ReadLine();
        }

        //Programa que a partir d’un paràmetre retorna com a Double, o un valor per defecte de 1.0 en cas de no poder-se.
        public void aDoubleoU()
        {
            string input = Console.ReadLine();

            double output;
            try
            {
                output = Convert.ToDouble(input);
                Console.WriteLine(output);
            }
            catch (FormatException e)
            {
                output = 1.0;
                Console.WriteLine(output);
                Console.WriteLine(e.Message);
            }
            Console.WriteLine("Finalizado");
            Console.ReadLine();

        }

        //Programa que mostra per pantalla el contingut d’un fitxer, però que si no existeix, mostra un missatge
        public void FtxerException()
        {
            try
            {
                string fileName = "fitxerInexistent.txt";
                string fileContent = File.ReadAllText(fileName);
                Console.WriteLine(fileContent);
            }
            catch (IOException ex)
            {
                Console.WriteLine("S'ha produït un error d'entrada/sortida:");
                Console.WriteLine(ex.Message);
            }
            Console.ReadLine();

        }

        //Programa que faservir un objecte MutableList, afegeixi i esborri elements, i mitjançant try-catch fa el control de les excepcions. 

        public void LliureAplic()
        {
            List<string> myList = new List<string>();

            try
            {
                
                myList.Add("Elemento 1");
                myList.Add("Elemento 2");
                myList.Add("Elemento 3");

                myList.RemoveAt(1);

                // Intentar eliminar un elemento que no existe
                myList.RemoveAt(10);

                // Agregar un elemento nulo a la lista
                myList.Add(null);

            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.WriteLine("Error: El índice está fuera del rango válido "+ ex.Message);
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine("Error: El valor del elemento es nulo "+ ex.Message);
            }

            // Imprimir los elementos de la lista
            Console.WriteLine("Elementos en la lista:");
            foreach (string element in myList)
            {
                Console.WriteLine(element);
            }

            Console.ReadKey();
        }
    }
    
}
